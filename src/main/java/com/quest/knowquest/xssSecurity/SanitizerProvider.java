package com.quest.knowquest.xssSecurity;

import org.owasp.html.PolicyFactory;

public interface SanitizerProvider {
    PolicyFactory getPolicyFactory();

}
