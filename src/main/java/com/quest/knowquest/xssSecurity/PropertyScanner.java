package com.quest.knowquest.xssSecurity;

public interface PropertyScanner {
    Object scan(Object obj);
}
