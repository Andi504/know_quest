# Use the official OpenJDK base image with Java 17
FROM openjdk:17-alpine

# Set the working directory in the container
WORKDIR /app

# Copy the packaged jar file into the container
COPY target/know-quest-0.0.1-SNAPSHOT.jar /app/know-quest.jar

# Expose the port the app runs on (optional)
EXPOSE 8080

# Command to run the jar file
CMD ["java", "-jar", "know-quest.jar"]
